#include <stdio.h>

#include <gmp.h>
#include <mpfr.h>
#include <time.h>

int main (void)
{
  unsigned int i;
  mpfr_t s, t, u;

  mpfr_init2 (t, 200);
  mpfr_set_str (t, "111111111111111111111111111111111111111111111111111111111111", 10, MPFR_RNDD);
  mpfr_init2 (s, 200);
  mpfr_set_str (s, "333333333333333333333333333333333333333333333333333333333333", 10, MPFR_RNDD);
  mpfr_init2 (u, 200);
  clock_t         start; // for timing
  // for (i = 1; i <= 100; i++)
  //   {
      // mpfr_mul_ui (t, t, i, MPFR_RNDU);
      // mpfr_set_d (u, 1.0, MPFR_RNDD);
      // mpfr_div (u, u, t, MPFR_RNDD);
  start = clock();
      mpfr_mul (u, s, t, MPFR_RNDD);
  start = clock() - start;
  printf(" %f ms \n", 1000 * (double) start / CLOCKS_PER_SEC);
  // }
  printf ("Sum is ");
  mpfr_out_str (stdout, 10, 0, u, MPFR_RNDD);
  putchar ('\n');
  mpfr_clear (s);
  mpfr_clear (t);
  mpfr_clear (u);
  return 0;
}